﻿using System;
using System.Runtime.Serialization;

namespace KattisTest {
    [Serializable]
    public class TestException : Exception {
        public TestStage Stage { get; private set; }
        public TestException() { }
        public TestException(string message, TestStage stage = TestStage.Unknown) : base(message) {
            Stage = stage;
        }
        public TestException(string message) : base(message) { }
        public TestException(string message, Exception inner) : base(message, inner) { }
        protected TestException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
