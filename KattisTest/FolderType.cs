﻿namespace KattisTest {
    public enum FolderType {
        Unknown = 0,
        GitFolder,
        CPlusPlus,
        C,
        CSharp,
        Ruby,
        Cobol,
        Haskell,
        Java,
        NodeJS,
        SpiderMonkey,
        CommonLisp,
        ObjectiveC,
        OCaml,
        Pascal,
        Prolog,
        Python2,
        Python3,
        Go,
        Kotlin,
        PHP,
        Rust
    }
}