﻿namespace KattisTest {
    public enum TestStage {
        Unknown = 0,
        Preparation,
        Compilation,
        Testing
    }
}