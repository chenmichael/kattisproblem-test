﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
#nullable enable

namespace KattisTest {
    [Serializable]
    public class KattisTestConfig {
        private static string Url(string problemName) => $"https://open.kattis.com/problems/{problemName}/file/statement/samples.zip";
#pragma warning disable CA2235 // Mark all non-serializable fields
        public string ProblemName;
        public List<SampleDataSet> SampleData;
#pragma warning restore CA2235 // Mark all non-serializable fields
        private const string problemCacheDirName = "problemcache";
        public KattisTestConfig(string problemName, IEnumerable<SampleDataSet> sets) {
            ProblemName = problemName;
            SampleData = sets.ToList();
        }

        public static KattisTestConfig ForFile(string filepath) {
            var problemname = Path.GetFileNameWithoutExtension(filepath).ToLower();
            var cachefile = Path.Combine(Environment.CurrentDirectory, problemCacheDirName, $"{problemname}.kts");
            return ForceCacheOrNew(problemname, cachefile);
        }

        private static KattisTestConfig ForceCacheOrNew(string problemname, string cachefile) {
            try {
                var config = ReadCache(cachefile);
                Console.WriteLine("Grabbed problem sample data from cache!");
                return config;
            } catch (FileNotFoundException) {
            } catch (Exception e) {
                try {
                    File.Delete(cachefile);
                    Console.WriteLine("Cache file deleted!");
                } catch (Exception) {
                    Console.WriteLine("Cannot delete cache file!");
                }
                Console.WriteLine("Error reading cache: {0}", e.Message);
            }
            var newconfig = DownloadProblem(problemname);
            TryToCache(newconfig, cachefile);
            return newconfig;
        }

        private static void TryToCache(KattisTestConfig config, string cachefile) {
            try {
                using var fs = new FileStream(cachefile, FileMode.Create);
                var formatter = new BinaryFormatter();
                formatter.Serialize(fs, config);
            } catch (Exception e) {
                Console.WriteLine("Warning: cannot write to cache: {0}", e.Message);
            }
        }

        private static KattisTestConfig ReadCache(string cachefile) {
            // Will throw file not found exception!
            using var fs = new FileStream(cachefile, FileMode.Open);
            var formatter = new BinaryFormatter();
            return (KattisTestConfig)formatter.Deserialize(fs);
        }

        private static KattisTestConfig DownloadProblem(string problemname) {
            Console.WriteLine("Downloading sample files for {0}", problemname);
            var request = WebRequest.Create(Url(problemname));
            using var response = request.GetResponse();
            Console.WriteLine("Read ZIP archive");
            using var archive = new ZipArchive(response.GetResponseStream(), ZipArchiveMode.Read, false);
            var sets = ExtractArchive(archive).ToList();
            return new KattisTestConfig(problemname, sets);
        }

        private static IEnumerable<SampleDataSet> ExtractArchive(ZipArchive archive) {
            foreach (var entry in archive.Entries) {
                var datasetName = Path.GetFileNameWithoutExtension(entry.Name);
                var extension = Path.GetExtension(entry.Name);
                switch (extension) {
                case ".in":
                    var answer = archive.Entries.Where(i => Path.GetFileNameWithoutExtension(i.Name) == datasetName && Path.GetExtension(i.Name) == ".ans").FirstOrDefault();
                    if (answer is null)
                        Console.WriteLine("- Found sample input without output: {0}", entry.FullName);
                    else {
                        Console.WriteLine("- Found sample data set: {0}", datasetName);
                        using var inputStream = entry.Open();
                        using var outputStream = answer.Open();
                        using var inReader = new StreamReader(inputStream);
                        using var outReader = new StreamReader(outputStream);
                        yield return new SampleDataSet(datasetName, inReader, outReader);
                    }
                    break;
                case ".ans":
                    break;
                default:
                    Console.WriteLine("- Found unknown file in archive: {0}", entry.FullName);
                    break;
                }
            }
        }
    }
}