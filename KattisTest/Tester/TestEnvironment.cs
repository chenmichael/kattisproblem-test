﻿using DiffPlex;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace KattisTest.Tester {
    abstract class TestEnvironment {
        private static readonly Dictionary<FolderType, Func<TestEnvironment>> environmentCreators = new Dictionary<FolderType, Func<TestEnvironment>>() {
            [FolderType.CPlusPlus] = () => new CPlusPlusTester()
        };
        private const int testTimeout = 5000;

        public static TestEnvironment GetEnvironment(FolderType type) {
            if (environmentCreators.TryGetValue(type, out var create))
                return create();
            else
                throw new ArgumentOutOfRangeException("No environment creator exists for this environment!");
        }
        protected const string linesep = "------------------------------------------------------------";
        public abstract string GetName();
        public abstract bool IsLanguage(string dirname);
        public abstract bool IsTestSubject(string filename);
        public abstract void TestFile(string filename);
        public virtual void Test(string executable, string input, string expectedoutput) {
            var info = new ProcessStartInfo(executable) {
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                UseShellExecute = false
            };
            var process = Process.Start(info);
            using var output = process.StandardOutput.ReadToEndAsync();
            using var error = process.StandardError.ReadToEndAsync();
            process.StandardInput.Write(input);
            process.StandardInput.Flush();
            process.StandardInput.Close();
            if (process.WaitForExit(testTimeout)) {
                if (process.ExitCode == 0) {
                    Console.WriteLine("Programm executed successfully", process.ExitCode, executable);
                    Compare(output.Result, expectedoutput);
                } else {
                    Console.WriteLine("Program error output was:\n{0}", error.Result);
                    throw new TestException($"Runtime error: exit code {process.ExitCode}", TestStage.Testing);
                }
            } else
                throw new TestException($"Timeout of {testTimeout} milliseconds reached!", TestStage.Testing);
        }

        private void Compare(string result, string expectedoutput) {
            result = result.Trim();
            expectedoutput = expectedoutput.Trim();
            var diffBuilder = new InlineDiffBuilder(new Differ());
            var diff = diffBuilder.BuildDiffModel(expectedoutput, result);

            var color = Console.ForegroundColor;

            var different = false;

            foreach (var line in diff.Lines) {
                if (line.Type != ChangeType.Unchanged)
                    if (line.Position.HasValue)
                        Console.Write("At {0}:", line.Position.Value);
                switch (line.Type) {
                case ChangeType.Inserted:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("+ ");
                    different = true;
                    break;
                case ChangeType.Modified:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("~ ");
                    different = true;
                    break;
                case ChangeType.Deleted:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("- ");
                    different = true;
                    break;
                case ChangeType.Imaginary:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("? ");
                    different = true;
                    break;
                default:
                    continue;
                }

                Console.WriteLine(line.Text);
            }
            Console.ForegroundColor = color;

            if (different)
                throw new TestException("Difference in output!", TestStage.Testing);
        }
    }
}
