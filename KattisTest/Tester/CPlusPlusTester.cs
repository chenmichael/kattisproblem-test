﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace KattisTest.Tester {
    class CPlusPlusTester : TestEnvironment {
        private const string compiler = "g++";
        private const int compileTimeout = 5000;
        private const string environmentname = "C++ Test Environment";
        private readonly List<string> extensions = new List<string> { "cpp" };

        public override string GetName() => environmentname;

        public override bool IsLanguage(string dirname) => extensions.Contains(Path.GetFileName(dirname));

        public override bool IsTestSubject(string filepath) => extensions.Contains(Path.GetExtension(filepath).TrimStart('.'));

        public override void TestFile(string filepath) {
            Console.WriteLine(linesep);
            Console.WriteLine("Stage 1: Preparation");
            Console.WriteLine(linesep);
            KattisTestConfig testConfig;
            try {
                testConfig = KattisTestConfig.ForFile(filepath);
            } catch (TestException) {
                throw;
            } catch (Exception e) {
                throw new TestException($"Unhandled preparation exception: {e.Message}", TestStage.Preparation);
            }

            Console.WriteLine();
            Console.WriteLine(linesep);
            Console.WriteLine("Stage 2: Compilation");
            Console.WriteLine(linesep);
            string executable;
            try {
                executable = Compile(filepath);
            } catch (TestException) {
                throw;
            } catch (Exception e) {
                throw new TestException($"Unhandled compiler exception: {e.Message}", TestStage.Compilation);
            }

            Console.WriteLine();
            Console.WriteLine(linesep);
            Console.WriteLine("Stage 3: Testing");
            Console.WriteLine(linesep);
            try {
                foreach (var testset in testConfig.SampleData) {
                    Console.WriteLine("Testing sample data set '{0}':", testset.Name);
                    Test(executable, testset.Input, testset.Answer);
                    Console.WriteLine("Test for '{0}' successful!", testset.Name);
                }
                Console.WriteLine("All Tests successful!");
            } catch (TestException) {
                throw;
            } catch (Exception e) {
                throw new TestException($"Unhandled testing exception: {e.Message}", TestStage.Testing);
            }
        }

        private string Compile(string filepath) {
            var outfile = Path.GetFileNameWithoutExtension(filepath);
            var info = new ProcessStartInfo(compiler, $"-g -o \"{outfile}\" -O2 -std=gnu++17 -static \"{filepath}\"") {
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };
            var process = Process.Start(info);
            var error = process.StandardError.ReadToEndAsync();
            if (process.WaitForExit(compileTimeout)) {
                if (process.ExitCode == 0) {
                    Console.WriteLine("{1} exited with code {0}!", process.ExitCode, compiler);
                    Console.WriteLine("Compilation successful! ({0} milliseconds)", process.TotalProcessorTime.TotalMilliseconds);
                    return outfile;
                } else
                    throw new TestException(error.Result, TestStage.Compilation);
            } else
                throw new TestException($"Timeout of {compileTimeout} milliseconds reached!", TestStage.Compilation);
        }
    }
}
