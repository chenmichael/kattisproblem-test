﻿using System;
using System.Collections.Generic;
using System.IO;
using KattisTest.Tester;

namespace KattisTest {
    public class TestLauncher {
        private readonly string workingDirectory;
        public TestLauncher(string dir) {
            workingDirectory = dir;
            if (!Directory.Exists(workingDirectory))
                throw new DirectoryNotFoundException($"'{Path.GetFileName(workingDirectory)}' wurde nicht gefunden!");
        }
        public static int Main(string[] args) {
            string dir;
            if (args.Length > 0)
                dir = args[0];
            else
                dir = Environment.CurrentDirectory;
            Console.WriteLine(linesep);
            Console.WriteLine("Kattis Test on folder '{0}'", Path.GetDirectoryName(dir));
            Console.WriteLine(linesep);
            Console.WriteLine();
            try {
                new TestLauncher(dir).Run();
            } catch (Exception e) {
                Console.WriteLine();
                Console.WriteLine(linesep);
                Console.WriteLine("! UNCAUGHT APPLICATION ERROR !");
                Console.WriteLine();
                Console.WriteLine(e);
                Console.WriteLine();
                Console.WriteLine(linesep);
                return -1;
            }
            Console.WriteLine();
            Console.WriteLine(linesep);
            Console.WriteLine("Tests complete!");
            Console.WriteLine();
            Console.WriteLine(linesep);
            return 0;
        }
        private void Run() {
            var successful = new List<string>();
            var unsuccessful = new Dictionary<string, TestException>();

            foreach (var directory in Directory.GetDirectories(workingDirectory)) {
                var dirName = Path.GetFileName(directory);
                var folderType = ParseLanguage(dirName);
                switch (folderType) {
                case FolderType.CPlusPlus:
                case FolderType.CSharp:
                    TestFolder(directory, folderType, successful, unsuccessful);
                    break;
                case FolderType.GitFolder:
                    break;
                default:
                    Console.WriteLine("Skipping unsupported language '{0}'!", dirName);
                    break;
                }
            }

            Console.WriteLine();
            Console.WriteLine(linesep);
            Console.WriteLine("Result overview:");
            Console.WriteLine(linesep);
            Console.WriteLine();
            Console.WriteLine(linesep);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Unsuccessful tests:");
            foreach (var file in unsuccessful)
                Console.WriteLine("- {0} at Stage {1}\n\tMessage: {2}", file.Key, file.Value.Stage, file.Value.Message);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(linesep);
            Console.WriteLine();
            Console.WriteLine(linesep);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Successful tests:");
            foreach (var file in successful)
                Console.WriteLine("- {0}", file);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private FolderType ParseLanguage(string dirName) {
            return (dirName.ToLower()) switch
            {
                ".git" => FolderType.GitFolder,
                "cpp" => FolderType.CPlusPlus,
                //"cs" => FolderType.CSharp,
                _ => FolderType.Unknown,
            };
        }
        protected const string linesep = "------------------------------------------------------------";
        private void TestFolder(string directory, FolderType type, List<string> successful, Dictionary<string, TestException> unsuccessful) {
            var environment = TestEnvironment.GetEnvironment(type);
            Console.WriteLine(linesep);
            Console.WriteLine("Testing {0} using {1}...", Path.GetFileName(directory), environment.GetName());
            Console.WriteLine(linesep);

            var first = true;
            foreach (var filepath in Directory.EnumerateFiles(directory)) {
                var filename = Path.GetFileName(filepath);
                if (!environment.IsTestSubject(filepath))
                    continue;

                Console.WriteLine();
                if (first)
                    first = false;
                else {
                    Console.WriteLine(linesep);
                    Console.WriteLine();
                }

                try {
                    environment.TestFile(filepath);
                    Console.WriteLine("TESTING SUCCESSFUL");
                    successful.Add(filename);
                } catch (TestException e) {
                    unsuccessful.Add(filename, e);
                    if (e.Stage == TestStage.Unknown)
                        Console.WriteLine("TESTING FAILED FATALLY\nReason:\n{0}", e.Message);
                    else
                        Console.WriteLine("TESTING FAILED on stage '{0}', Reason:\n{1}", e.Stage, e.Message);
                }
            }
        }
    }
}
