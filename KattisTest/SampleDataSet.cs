﻿using System;
using System.IO;
using System.IO.Compression;

namespace KattisTest {
    [Serializable]
    public class SampleDataSet {
        public SampleDataSet(string datasetName, string input, string answer) {
            Name = datasetName;
            Input = input;
            Answer = answer;
        }

        public SampleDataSet(string datasetName, StreamReader inputStream, StreamReader outputStream) :
            this(datasetName, inputStream.ReadToEnd(), outputStream.ReadToEnd()) { }

#pragma warning disable CA2235 // Mark all non-serializable fields
        public string Name { get; set; }
        public string Input { get; set; }
        public string Answer { get; set; }
#pragma warning restore CA2235 // Mark all non-serializable fields
    }
}